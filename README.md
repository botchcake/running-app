# Running app.

Android app to support my running efforts.

I couldn't find a solid android app that supported setting my own runing times,
I've been using a start-to-run schema that wasn't supported. So I created this.

This is open source and will only be developed to suit my own needs an nothing 
more. Feel free to open issues and/or pull requests but I will decline them if
I feel they enlarge the scope too much.

This project is also used to increase my knowledge of java/kotlin and thus if 
you find things that are not ideomatic kotlin - please feel free to point them
out.


## License

If you plan on making money of selling this app, don't. Otherwise feel free to
do whatever.
