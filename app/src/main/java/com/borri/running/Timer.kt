package com.borri.running

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.github.kimkevin.cachepot.CachePot
import kotlinx.android.synthetic.main.activity_timer.*
import android.media.RingtoneManager
import android.os.CountDownTimer

class Timer : AppCompatActivity() {

    private val handler = Handler()

    private var fullTimeTimer: CountDownTimer? = null
    private var activityTimeTimer: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timer)

        val details = CachePot.getInstance().pop<DayDetails>("details")
        if (details == null) {
            return
        }

        Toast.makeText(this, " - START - ", Toast.LENGTH_SHORT).show()
        chronometer.start()

        var timer = 0
        for (item: Sequence in details.getSequences()) {
            createActivityTimerRunnable(convertToMicroSeconds(timer), item.activityDuration)
            timer += item.activityDuration
            createTimerRunnable(convertToMicroSeconds(timer), "Stop " + item.activityType)
        }

        startFullTimeTimer(convertToMicroSeconds(timer).toLong())
    }

    /**
     * Method to start count down timer. This is for the full time.
     */
    private fun startFullTimeTimer(Time: Long) {
        fullTimeCircle.max = Time.toInt() / 1000
        fullTimeCircle.progress = Time.toInt() / 1000

        fullTimeTimer = object : CountDownTimer(Time, 500) {
            override fun onTick(millisUntilFinished: Long) {
                fullTimeCircle.progress = (millisUntilFinished / 1000).toInt()
            }

            override fun onFinish() {
                createTimerRunnable(600, " - STOP - ")
                chronometer.stop()
            }

        }.start()
        fullTimeTimer?.start()
    }

    /**
     * Method to start count down timer. This is for a single activity.
     */
    private fun startActivityTimer(Time: Long) {
        activityTimeCircle.max = Time.toInt() / 1000
        activityTimeCircle.progress = Time.toInt() / 1000
        activityTimeTimer = object : CountDownTimer(Time, 500) {
            override fun onTick(millisUntilFinished: Long) {
                activityTimeCircle.progress = (millisUntilFinished / 1000).toInt()
            }

            override fun onFinish() {
                activityTimeCircle.progress = 0
            }

        }.start()
        activityTimeTimer?.start()
    }

    /**
     * Convert a time (as given in minutes) to a time useful in microseconds.
     *
     * We multiply by 60 (for seconds), and by 1000 for microseconds.
     */
    private fun convertToMicroSeconds(input: Int): Int = input * 60 * 1000

    /**
     * Create a runnable item, this is a timer that sets a toast + notification sound.
     */
    private fun createTimerRunnable(notificationTime: Int, message: String) {
        val event = Runnable {
            run {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val r = RingtoneManager.getRingtone(applicationContext, notification)
                r.play()
            }
        }

        handler.postAtTime(event, System.currentTimeMillis() + notificationTime)
        handler.postDelayed(event, notificationTime.toLong())
    }

    /**
     * Create activity timer runnable.
     */
    private fun createActivityTimerRunnable(timer: Int, activityDuration: Int) {
        val event = Runnable {
            run {
                startActivityTimer(convertToMicroSeconds(activityDuration).toLong())
            }
        }
        handler.postAtTime(event, System.currentTimeMillis() + timer)
        handler.postDelayed(event, timer.toLong())
    }

}
