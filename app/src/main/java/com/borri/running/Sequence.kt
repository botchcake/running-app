package com.borri.running

data class Sequence(val activityType: String, val activityDuration: Int)
