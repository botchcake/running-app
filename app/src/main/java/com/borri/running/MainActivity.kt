package com.borri.running

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.content_main.*
import android.content.Intent
import com.github.kimkevin.cachepot.CachePot

class MainActivity : AppCompatActivity() {

    private var details = DayDetails()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val potDetails = CachePot.getInstance().pop<DayDetails>("details")
        if (potDetails != null) {
            details = potDetails
        }

        button.setOnClickListener {
            var repeatSequence: Int = repeat.editText?.text.toString().toInt()
            while (repeatSequence > 0) {
                repeatSequence--
                addSequences("Running", runtime.editText?.text.toString().toInt())
                addSequences("Walking", walktime.editText?.text.toString().toInt())
            }
            sendToNextPage()
        }
    }

    /**
     * Add a sequence to the day details.
     */
    private fun addSequences(type: String, time: Int) {
        val sequence = Sequence(type, time)
        details.addSequence(sequence)
    }

    /**
     * Go to the next screen.
     */
    private fun sendToNextPage() {
        CachePot.getInstance().push("details", details)
        val intent = Intent(this, DisplayMessageActivity::class.java)
        startActivity(intent)
    }

}
