package com.borri.running

class DayDetails : Any() {

    private var sequences: Array<Sequence> = Array(0, { Sequence("foo", 0) })

    fun addSequence(sequence: Sequence) {
        sequences = sequences.plus(sequence)
    }

    fun getSequences(): Array<Sequence> = sequences

    fun totalTimeMessage(): String {
        val totalTime = sequences
                .filter { it.activityType !== "foo" }
                .sumBy { it.activityDuration }
        return "Total: $totalTime m"
    }

    fun breakDown(): String {
        var output = ""
        sequences
                .asSequence()
                .filter { it.activityType !== "foo" }
                .forEach { output = "$output${it.activityType}: ${it.activityDuration}min.\n"
                }

        return output
    }

}
