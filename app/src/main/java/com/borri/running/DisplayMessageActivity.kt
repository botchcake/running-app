package com.borri.running

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.github.kimkevin.cachepot.CachePot
import kotlinx.android.synthetic.main.activity_display_message.*

class DisplayMessageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_message)

        // Get data from the cache + set it again for the next view.
        val details = CachePot.getInstance().pop<DayDetails>("details")

        titleText.text = details.totalTimeMessage()
        textView.text = details.breakDown()

        start_running.setOnClickListener {
            val intent = Intent(this, Timer::class.java)
            startActivity(intent)
        }
        back_start.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        CachePot.getInstance().push("details", details)
    }

}
