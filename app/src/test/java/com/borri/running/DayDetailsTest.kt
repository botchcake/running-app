package com.borri.running

import org.junit.Test

import org.junit.Assert.*

/**
 * Local unit tet for DayDetails.
 */
class DayDetailsTest {

    @Test
    fun testAddSequence() {
        val sequence = Sequence("walking", 2)
        val details = DayDetails()
        details.addSequence(sequence)

        assertEquals("Total: 2 m", details.totalTimeMessage())
    }

    @Test
    fun testAddMultipleSequence() {
        val details = createDayDetails()

        assertEquals("Total: 4 m", details.totalTimeMessage())
    }

    @Test
    fun testBreakdown() {
        val details = createDayDetails()

        assertEquals("walking: 2min.\nrunning: 2min.\n", details.breakDown())
    }

    @Test
    fun testGetSequences() {
        val sequence = Sequence("walking", 2)
        val sequence2 = Sequence("running", 2)

        var sequenceArray: Array<Sequence> = Array(1, { sequence })
        sequenceArray = sequenceArray.plus(sequence2)

        val details = createDayDetails()
        assertArrayEquals(sequenceArray, details.getSequences())
    }

    private fun createDayDetails(): DayDetails {
        val sequence = Sequence("walking", 2)
        val sequence2 = Sequence("running", 2)
        val details = DayDetails()
        details.addSequence(sequence)
        details.addSequence(sequence2)
        return details
    }

}
